const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    header = require('gulp-header'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    uglify = require('gulp-uglify'),
    pkg = require('./package.json'),
    stripCssComments = require('gulp-strip-css-comments'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    include = require("gulp-include"),
    css_import = require("gulp-cssimport");


const dirs = {
    'src': './src',
    'dist_admin': './app/core/static/core_admin',
    'vendors_admin': './app/core/static/core_admin/vendors',
};

// Set the banner content
const banner = ['/*!\n',
    ' * <%= pkg.title %> v<%= pkg.version %> \n',
    ' * Copyright ' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' */\n',
    ''
].join('');

// Compiles SCSS files from /scss into /css
gulp.task('admin:sass', function () {
    return gulp.src(dirs.src + '/admin/scss/main.scss')
        .pipe(sass({
            errLogToConsole: true,
            includePaths: [
                dirs.vendors_admin + "/material-lite/",
            ]
        }))
        .pipe(stripCssComments({all: true}))
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(gulp.dest(dirs.src + '/admin/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});


// Minify compiled CSS
gulp.task('admin:minify-css', ['admin:sass'], function () {
    return gulp.src(dirs.src + '/admin/css/main.css')
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(rename({
            basename: "main",
            suffix: '.min'
        }))
        .pipe(gulp.dest(dirs.dist_admin + '/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Minify custom JS
gulp.task('admin:minify-js', function () {
    return gulp.src(dirs.src + '/admin/js/main.js')
        .pipe(include({
            extensions: "js",
            hardFail: true,
            includePaths: [
                dirs.vendors
            ]
        }))
        .pipe(uglify())
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(rename({
            basename: "main",
            suffix: '.min'
        }))
        .pipe(gulp.dest(dirs.dist_admin + '/js'))
        .pipe(browserSync.reload({
            stream: true
        }));
});


gulp.task('admin:copy', function () {

    gulp.src([
        dirs.src + '/theme/modular-admin-html/dist/**/*',
    ]).pipe(gulp.dest(dirs.vendors_admin + '/modular-admin'));

    gulp.src([
        'node_modules/jquery-mask-plugin/dist/**/*',
    ]).pipe(gulp.dest(dirs.vendors_admin + '/jquery-mask-plugin'));

});

// Configure the browserSync task
gulp.task('browserSync', function () {
    browserSync.init({
        notify: false,
        proxy: "localhost:8000"
    });
});


// Default task
gulp.task('admin', ['admin:sass', 'admin:minify-css', 'admin:minify-js', 'admin:copy']);

// Dev task with browserSync
gulp.task('admin:watch', ['browserSync', 'admin:sass', 'admin:minify-css', 'admin:minify-js'], function () {
    gulp.watch(dirs.src + '/admin/scss/*.scss', ['admin:sass']);
    gulp.watch(dirs.src + '/admin/css/*.css', ['admin:minify-css']);
    gulp.watch(dirs.src + '/admin/js/*.js', ['admin:minify-js']);
    gulp.watch(dirs.src + '/admin/js/**/*.js', browserSync.reload);
});