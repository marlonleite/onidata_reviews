from django.contrib.auth.views import login, logout
from django.urls import path

app_name = 'registers'

from . import views

urlpatterns = [
    path('empresas/', views.CompanyListView.as_view(), name='company_index'),
    path('empresas/adicionar/', views.CompanyCreateView.as_view(), name='company_create'),
    path('empresas/<int:pk>/atualizar/', views.CompanyUpdateView.as_view(), name='company_update'),
    path('empresas/<int:pk>/apagar/', views.CompanyDeleteView.as_view(), name='company_delete'),

    path('revisoes/', views.ReviewListView.as_view(), name='review_index'),
    path('revisoes/adicionar/', views.ReviewCreateView.as_view(), name='review_create'),
    path('revisoes/<int:pk>/atualizar/', views.ReviewUpdateView.as_view(), name='review_update'),
    path('revisoes/<int:pk>/apagar/', views.ReviewDeleteView.as_view(), name='review_delete'),

]
