from django import forms
from .models import Company, Review


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = '__all__'


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = '__all__'
