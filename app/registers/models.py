
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.urls import reverse

from app.accounts.models import User


class Company(models.Model):
    """
    Company Model.
    """
    name = models.CharField('Nome', max_length=100)
    email = models.EmailField('E-mail')

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    modified_at = models.DateTimeField('Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('registers:company_update', kwargs={'pk': self.pk})


class Review(models.Model):
    """
      Review Model.
    """
    title = models.CharField('Titulo', max_length=100)
    summary = models.TextField('Conteúdo')
    rating = models.PositiveIntegerField('Avaliação', validators=[MinValueValidator(1), MaxValueValidator(5)])
    ip_address = models.GenericIPAddressField('Endereço de IP')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, verbose_name='Empresa')
    reviewer = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Revisor')
    submission_at = models.DateTimeField('Data de submissão', auto_now_add=True)

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    modified_at = models.DateTimeField('Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'Revisão'
        verbose_name_plural = 'Revisões'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('registers:review_update', kwargs={'pk': self.pk})
