from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from app.accounts.models import User, Reviewer
from ..models import Company


class CompanyTestCase(TestCase):

    def setUp(self):
        try:
            self.reviewer = Reviewer.objects.get(user__username="teste")
        except Reviewer.DoesNotExist:
            self.user = User.objects.create(
                name="teste", email="teste@teste.com", username="teste",
                password="teste", is_reviewer=True
            )
            self.reviewer = Reviewer.objects.create(
                user=self.user
            )

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_company_model(self):
        self.assertTrue(Company.objects.create(name="teste", email="teste@teste.com"))

    def test_string_representation(self):
        company = Company(name="teste", email="teste")
        self.assertEqual(str(company), company.name)

    def test_verbose_name_plural(self):
        self.assertEqual(str(Company._meta.verbose_name_plural), "Empresas")

    def test_verbose_name(self):
        self.assertEqual(str(Company._meta.verbose_name), "Empresa")

    def test_company_get_success_companies_status_code(self):
        url = reverse('registers:company_index')
        request = self.client.get(url)
        self.assertEquals(request.status_code, status.HTTP_200_OK)

    def tearDown(self):
        Company.objects.all().delete()
