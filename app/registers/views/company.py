from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from ..forms import CompanyForm
from ..models import Company

model = Company
form = CompanyForm
label = 'Empresas'
warning_message = 'Você não tem permissão para gerenciar ' + label.lower() + "."


class CompanyListView(LoginRequiredMixin, ListView):
    model = model
    template_name = 'registers/companies/index.html'
    paginate_by = 10
    ordering = ['-created_at']

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label
        context["note_empty"] = 'Nenhuma ' + label + ' cadastrada.'

        return context


class CompanyCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = model
    template_name = 'registers/companies/add.html'
    form_class = form
    success_url = reverse_lazy('registers:company_index')
    success_message = "Adicionado com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label

        return context


class CompanyUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = model
    template_name = 'registers/companies/edit.html'
    form_class = form
    success_url = reverse_lazy('registers:company_index')
    success_message = "Atualizado com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label

        return context


class CompanyDeleteView(LoginRequiredMixin, DeleteView):
    model = model
    template_name = 'core_admin/confirm_delete.html'
    success_url = reverse_lazy('registers:company_index')
    success_message = "Removido com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(CompanyDeleteView, self).delete(request, *args, **kwargs)
