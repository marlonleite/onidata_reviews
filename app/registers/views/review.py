from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from ..forms import ReviewForm
from ..models import Review

model = Review
form = ReviewForm
label = 'Revisões'
warning_message = 'Você não tem permissão para gerenciar ' + label.lower() + "."


class ReviewListView(LoginRequiredMixin, ListView):
    model = model
    template_name = 'registers/reviews/index.html'
    paginate_by = 10
    ordering = ['-created_at']

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label
        context["note_empty"] = 'Nenhuma ' + label + ' cadastrada.'

        return context


class ReviewCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = model
    template_name = 'registers/reviews/add.html'
    form_class = form
    success_url = reverse_lazy('registers:review_index')
    success_message = "Adicionada com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label

        return context


class ReviewUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = model
    template_name = 'registers/reviews/edit.html'
    form_class = form
    success_url = reverse_lazy('registers:review_index')
    success_message = "Atualizada com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label

        return context


class ReviewDeleteView(LoginRequiredMixin, DeleteView):
    model = model
    template_name = 'core_admin/confirm_delete.html'
    success_url = reverse_lazy('registers:review_index')
    success_message = "Removida com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ReviewDeleteView, self).delete(request, *args, **kwargs)
