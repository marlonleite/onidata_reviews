from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken import views as rest_framework_views
from rest_framework.documentation import include_docs_urls

router = routers.DefaultRouter()

from . import views

router.register('empresas', views.CompanyViewSet, base_name='companies')
router.register('revisoes', views.ReviewViewSet, base_name='reviews')

urlpatterns = [
    path('', include(router.urls)),
    path('get-auth-token/', rest_framework_views.obtain_auth_token, name='get_auth_token'),
    path('adicionar/revisao/', views.ReviewCreateAPIView.as_view(), name='api_review_create'),
]
