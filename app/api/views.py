from rest_framework import status
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from app.core.utils import get_ip_address
from .serializers import *
from app.registers.models import Company, Review


class CompanyViewSet(ModelViewSet):
    """
    Companies List Api View
    """
    serializer_class = CompanySerializer
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    def get_queryset(self):
        queryset = Company.objects.all()
        return queryset


class ReviewViewSet(ModelViewSet):
    """
    Reviews List Api View
    """
    serializer_class = ReviewSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if self.request.user.is_reviewer:
            queryset = Review.objects.filter(reviewer=self.request.user)
        else:
            queryset = Review.objects.all()
        return queryset


class ReviewCreateAPIView(APIView):
    """
    Reviews Create Api View
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ReviewSerializer

    def post(self, request):

        if request.user.can_review:
            data = request.data

            data['reviewer'] = request.user
            data['ip_address'] = get_ip_address(request)

            serializer = self.serializer_class(data=data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

        else:
            return Response(status=HTTP_403_FORBIDDEN)
