from django.contrib.auth import get_user_model
from rest_framework import serializers

from app.accounts.views import User
from app.registers.models import Company, Review


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "email", "name"]


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    reviewer = UserSerializer()

    class Meta:
        model = Review
        fields = '__all__'
        depth = 2

    def create(self, validated_data):
        return Review.objects.create(**validated_data)