import os

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

from django.urls import reverse
from django.utils.timezone import now


def get_upload_path(instance, filename):
    return os.path.join('account/photos/', now().date().strftime("%Y/%m/%d"), filename)


class User(AbstractUser):
    """
    AbstractUser Class to extend User Model
    """
    photo = models.ImageField(upload_to=get_upload_path, blank=True, verbose_name='Foto')
    name = models.CharField('Nome', max_length=100, blank=True)
    is_reviewer = models.BooleanField('É revisor?', default=False)
    can_review = models.BooleanField('Pode revisar?', default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.name or self.username

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'


class PasswordReset(models.Model):
    """
    Reset UserPassword Model
    """
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name='Usuário',
        related_name='resets', on_delete=models.CASCADE
    )
    key = models.CharField('Chave', max_length=100, unique=True)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    confirmed = models.BooleanField('Confirmado?', default=False, blank=True)

    def __str__(self):
        return '{0} em {1}'.format(self.user, self.created_at)

    class Meta:
        verbose_name = 'Nova senha'
        verbose_name_plural = 'Novas senhas'
        ordering = ['-created_at']

