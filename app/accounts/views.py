from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import TemplateView, UpdateView, CreateView, DeleteView, ListView

from app.core.mixins import get_context_pagination
from app.registers.models import Review
from .forms import *
from .models import *


class DashbBoardView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/dashboard.html'

    reviews_paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(DashbBoardView, self).get_context_data(**kwargs)

        users = User.objects.filter(is_reviewer=True).order_by('-created_at')
        reviews = Review.objects.all().order_by('-created_at')

        if self.request.user.is_reviewer:
            reviews = Review.objects.filter(reviewer=self.request.user).order_by('-created_at')

        reviews_page_obj = get_context_pagination(
            self.request, "reviews_page", reviews, self.reviews_paginate_by
        )

        context['reviews_num'] = reviews.count()
        context['reviewers_num'] = users.count()
        context['reviews'] = reviews_page_obj

        return context


def register(request):
    template = 'accounts/register.html'
    context = {}
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            user = authenticate(
                username=user.username, password=form.cleaned_data['password1']
            )
            messages.success(
                request, 'Registro realizado com sucesso.'
            )
            login(request, user)
            return redirect('accounts:login')
    else:
        form = RegisterForm()
    context['form'] = form
    return render(request, template, context)


def password_reset(request):
    template_name = 'accounts/password_reset.html'
    context = {}
    form = PasswordResetForm(request.POST or None)
    if form.is_valid():
        form.save()
        context['success'] = True
    context['form'] = form
    return render(request, template_name, context)


def password_reset_confirm(request, key):
    template_name = 'accounts/password_reset_confirm.html'
    context = {}
    reset = get_object_or_404(PasswordReset, key=key)
    form = SetPasswordForm(user=reset.user, data=request.POST or None)
    if form.is_valid():
        form.save()
        context['success'] = True
    context['form'] = form
    return render(request, template_name, context)


@login_required
def edit_password(request):
    template = 'accounts/profile_edit_password.html'
    context = {}
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Sua senha foi alterada com sucesso. '
                         'Por favor faça um novo acesso com a senha atualizada.'
            )
            return redirect('accounts:profile_detail', request.user.pk)

    else:
        form = PasswordChangeForm(user=request.user)
    context['form'] = form
    return render(request, template, context)


class UserProfileUpdate(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'accounts/profile.html'
    context_object_name = 'user'
    form_class = UserForm
    success_url = reverse_lazy('accounts:dashboard')

    def get_object(self):
        return self.request.user


label = 'Revisores'
warning_message = 'Você não tem permissão para gerenciar ' + label.lower() + "."


class ReviewerListView(LoginRequiredMixin, ListView):
    template_name = 'accounts/reviewers/index.html'
    paginate_by = 10
    ordering = ['-date_joined']

    def get_queryset(self):
        queryset = User.objects.filter(is_reviewer=True).order_by('-date_joined')
        return queryset

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label
        context["note_empty"] = 'Nenhum ' + label + ' cadastrado.'

        return context


class ReviewerCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = User
    template_name = 'accounts/reviewers/add.html'
    form_class = ReviewerFormCreate
    success_message = "Adicionado com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label

        return context

    def form_valid(self, form):
        user = form.save()
        return redirect('accounts:reviewer_index')


class ReviewerUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    template_name = 'accounts/reviewers/edit.html'
    form_class = ReviewerFormUpdate
    success_url = reverse_lazy('accounts:reviewer_index')
    success_message = "Atualizado com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)

        context["label"] = label

        return context


class ReviewerDeleteView(LoginRequiredMixin, DeleteView):
    model = User
    template_name = 'core_admin/confirm_delete.html'
    success_url = reverse_lazy('accounts:reviewer_index')
    success_message = "Removida com sucesso."

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_reviewer:
            messages.warning(request, warning_message)
            return redirect('accounts:dashboard')
        return super(self.__class__, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ReviewerDeleteView, self).delete(request, *args, **kwargs)
