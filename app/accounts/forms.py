from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.db import transaction

from app.core.mail import send_mail_template
from app.core.mixins import ReadOnlyFieldsMixin
from app.core.utils import generate_hash_key
from .models import PasswordReset, User


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label='E-mail')

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            return email
        raise forms.ValidationError(
            'Nenhum usuário encontrado com este e-mail'
        )

    def save(self):
        user = User.objects.get(email=self.cleaned_data['email'])
        key = generate_hash_key(user.username)
        reset = PasswordReset(key=key, user=user)
        reset.save()
        template_name = 'accounts/password_reset_mail.html'
        subject = 'Criar nova senha Mangue Digital'
        context = {
            'reset': reset,
        }
        send_mail_template(subject, template_name, context, [user.email])


class RegisterForm(forms.ModelForm):
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirmação de Senha', widget=forms.PasswordInput
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('A confirmação não está correta')
        return password2

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = ['username', 'email']


class UserForm(ReadOnlyFieldsMixin, forms.ModelForm):
    readonly_fields = ('username',)

    class Meta:
        model = User
        fields = ['username', 'email', 'photo']


class ReviewerFormCreate(forms.ModelForm):
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirmação de Senha', widget=forms.PasswordInput
    )

    class Meta:
        model = User
        fields = ['name', 'username', 'password1', 'password2', 'email', 'can_review', 'photo']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('A confirmação não está correta')
        return password2

    def clean_email(self):
        email = self.cleaned_data['email']

        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Este email já está em uso.")
        return email

    def save(self, commit=True):
        user = super(ReviewerFormCreate, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.is_reviewer = True
            user.save()
        return user


class ReviewerFormUpdate(ReadOnlyFieldsMixin, forms.ModelForm):
    readonly_fields = ('username',)

    class Meta:
        model = User
        fields = ['name', 'username', 'email', 'can_review', 'photo']

    def clean_email(self):
        email = self.cleaned_data['email']

        if User.objects.filter(email=email).exclude(username=self.instance.username).exists():
            raise forms.ValidationError("Este email já está em uso.")
        return email
