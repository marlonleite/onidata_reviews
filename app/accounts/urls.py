from django.contrib.auth.views import login, logout
from django.urls import path, re_path

from . import views

app_name = 'accounts'

urlpatterns = [
    path('', views.DashbBoardView.as_view(), name='dashboard'),
    path('entrar/', login, {'template_name': 'accounts/login.html'}, name='login'),
    path('sair/', logout, {'next_page': 'accounts:dashboard'}, name='logout'),

    path('cadastre-se/', views.register, name='register'),
    path('nova-senha/', views.password_reset, name='password_reset'),
    re_path('confirmar-nova-senha/(?P<key>\w+)/', views.password_reset_confirm, name='password_reset_confirm'),
    path('perfil/', views.UserProfileUpdate.as_view(), name='profile'),

    path('cadastros/revisores/', views.ReviewerListView.as_view(), name='reviewer_index'),
    path('cadastros/revisores/adicionar/', views.ReviewerCreateView.as_view(), name='reviewer_create'),
    path('cadastros/revisores/<int:pk>/atualizar/', views.ReviewerUpdateView.as_view(), name='reviewer_update'),
    path('cadastros/revisores/<int:pk>/apagar/', views.ReviewerDeleteView.as_view(), name='reviewer_delete'),
]
